package com.example.rent.sda_004_pr_add_controls_programatically;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private RelativeLayout relativeLayoutOne;
    private LinearLayout linearLayoutOne;
    private int clickCounter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        clickCounter = 0;
        relativeLayoutOne = (RelativeLayout) findViewById(R.id.relative_layout_one);
        linearLayoutOne = (LinearLayout) findViewById(R.id.linear_layout_one);

    }

    // Przyklad 1 - Relative Layout (wymaga parametrów poniewaz bez nich nowo utworzona kontrolka sie nalozy)
//    public void dodajKontrolke(View v) {
//        // tworze nowy obiekt text view i ustawiam kontekst
//        TextView createdTextView = new TextView(relativeLayoutOne.getContext());
//        // tworze obiekt wewnetrznej klasy parametry (potrzebne sa bo to layout relatywny - bez tego bedzie sie nakladac
//        RelativeLayout.LayoutParams parametry = new RelativeLayout.LayoutParams(100, 100);
//        parametry.topMargin = 100 * clickCounter;
//
//        // ustawiam parametry mojego text view
//        createdTextView.setLayoutParams(parametry);
//        createdTextView.setText("pawel");
//
//        // dodaje text view do mojego layoutu
//        relativeLayoutOne.addView(createdTextView);
//
//        // licznik na koniec
//        clickCounter++;


    // Przyklad 2 - Linear Layout (dodawanie jak w linkedList mozna powiedziec dlatego nie potrzeba mieszac w parametrach)
    public void dodajKontrolke(View v) {
        final TextView createdTextView = new TextView(linearLayoutOne.getContext());
        createdTextView.setText("pawel");

        // użycie OnClickListener z dodatkowa funckja czyli klikanie w kontrolke
        createdTextView.setOnClickListener(new View.OnClickListener() {
            int licznik = 0;

            @Override
            public void onClick(View v) {
                createdTextView.setText("kontrolka zostala kliknieta" + (++licznik));
            }
        });

        linearLayoutOne.addView(createdTextView);
    }
}
